# README #

This project is intended to verify the effect in build time if functions where stored in Extension of a class rather than inside the class itself.

### The project has two scripts: ###
* makeMoreFunctions.sh
* makeMoreExtensions.sh

Both scripts modifies the ViewController class inside the ViewController_Extensions.swift. 

The makeMoreFunctions.sh script generates functions inside the ViewController class.

On the other hand, the makeMoreExtensions.sh script generates functions to the extension of the ViewController class.

### How to set up? ###

* Show the duration of builds in Xcode by executing this in the command line:

	defaults write com.apple.dt.Xcode ShowBuildOperationDuration -bool YES

* Execute the makeMoreFunctions.sh from the command line.
* Do a clean build, and record the build time
* Execute the makeMoreExentions.sh from the command line.
* Do a clean build, and record the build time
* Compare the build time results

Note: You may change the NUMBER_OF_ITERATIONS to generate more functions/extensions.  Make sure that the value is the same for makeMoreFunctions.sh and makeMoreExtensions.sh scripts.

WARNING: XCODE MAY CRASH IF THE VALUE IS TO LARGE :D
