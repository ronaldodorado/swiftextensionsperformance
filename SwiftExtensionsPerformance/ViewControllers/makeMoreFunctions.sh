#!/bin/bash

# Create a clone of ViewController.swift without a closing bracket of the class
LINE_END_OF_CLASS=47
DELETE_LINE=" "
sed "${LINE_END_OF_CLASS}s/.*/$DELETE_LINE/" ViewController.swift > "ViewController_Extensions.swift"

# Do a for loop to insert functions to the class
NUMBER_OF_ITERATIONS=2000
for i in $(seq 1 $NUMBER_OF_ITERATIONS); do

NEW_LINE_1="    func GeneratedMethod_${i}() {"
NEW_LINE_2="	    print(\"GeneratedMethod_${i}\")"
NEW_LINE_3="	    print(\"GeneratedMethod_${i}\")"
NEW_LINE_4="	    print(\"GeneratedMethod_${i}\")"
NEW_LINE_5="    }"


echo "" >> "ViewController_Extensions.swift"
echo "$NEW_LINE_1" >> "ViewController_Extensions.swift"
echo "$NEW_LINE_2" >> "ViewController_Extensions.swift"
echo "$NEW_LINE_3" >> "ViewController_Extensions.swift"
echo "$NEW_LINE_4" >> "ViewController_Extensions.swift"
echo "$NEW_LINE_5" >> "ViewController_Extensions.swift"

done

# End the class with a bracket
echo "}" >> "ViewController_Extensions.swift"


