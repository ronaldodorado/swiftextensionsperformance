//
//  ViewController.swift
//  allenabled
//
//  Created by Ronaldo II Dorado on 2/12/16.
//  Copyright © 2016 Ronaldo II Dorado. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
    
    let stackView = UIStackView()
    let view1 = UIView()
    let view2 = UIView()
    let adapter = ViewAdapter()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        buildView()
        buildConstraints()
        setupStackView(stackView)
    }

    private func buildView() {
        view.backgroundColor = .white
        view.addSubview(stackView)
    }
    
    private func setupStackView(_ view: UIStackView) {
        view1.backgroundColor = .blue
        view2.backgroundColor = .gray
        
        stackView.axis = .vertical
        stackView.addArrangedSubview(view1)
        stackView.addArrangedSubview(view2)
        stackView.translatesAutoresizingMaskIntoConstraints = false;
        stackView.distribution = .fillProportionally
    }
    
    private func buildConstraints() {
        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

extension ViewController {
    func GeneratedMethod_1() {
	    print("GeneratedMethod_1")
	    print("GeneratedMethod_1")
	    print("GeneratedMethod_1")
    }
}

extension ViewController {
    func GeneratedMethod_2() {
	    print("GeneratedMethod_2")
	    print("GeneratedMethod_2")
	    print("GeneratedMethod_2")
    }
}

extension ViewController {
    func GeneratedMethod_3() {
	    print("GeneratedMethod_3")
	    print("GeneratedMethod_3")
	    print("GeneratedMethod_3")
    }
}

extension ViewController {
    func GeneratedMethod_4() {
	    print("GeneratedMethod_4")
	    print("GeneratedMethod_4")
	    print("GeneratedMethod_4")
    }
}

extension ViewController {
    func GeneratedMethod_5() {
	    print("GeneratedMethod_5")
	    print("GeneratedMethod_5")
	    print("GeneratedMethod_5")
    }
}

extension ViewController {
    func GeneratedMethod_6() {
	    print("GeneratedMethod_6")
	    print("GeneratedMethod_6")
	    print("GeneratedMethod_6")
    }
}

extension ViewController {
    func GeneratedMethod_7() {
	    print("GeneratedMethod_7")
	    print("GeneratedMethod_7")
	    print("GeneratedMethod_7")
    }
}

extension ViewController {
    func GeneratedMethod_8() {
	    print("GeneratedMethod_8")
	    print("GeneratedMethod_8")
	    print("GeneratedMethod_8")
    }
}

extension ViewController {
    func GeneratedMethod_9() {
	    print("GeneratedMethod_9")
	    print("GeneratedMethod_9")
	    print("GeneratedMethod_9")
    }
}

extension ViewController {
    func GeneratedMethod_10() {
	    print("GeneratedMethod_10")
	    print("GeneratedMethod_10")
	    print("GeneratedMethod_10")
    }
}
