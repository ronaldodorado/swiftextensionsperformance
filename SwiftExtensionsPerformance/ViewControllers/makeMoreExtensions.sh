#!/bin/bash

# Create a clone of ViewController.swift
sed "" ViewController.swift > "ViewController_Extensions.swift"

# Do a for loop to insert functions to the class
NUMBER_OF_ITERATIONS=2000
for i in $(seq 1 $NUMBER_OF_ITERATIONS); do

NEW_LINE_1="extension ViewController {"
NEW_LINE_2="    func GeneratedMethod_${i}() {"
NEW_LINE_3="	    print(\"GeneratedMethod_${i}\")"
NEW_LINE_4="	    print(\"GeneratedMethod_${i}\")"
NEW_LINE_5="	    print(\"GeneratedMethod_${i}\")"
NEW_LINE_6="    }"
NEW_LINE_7="}"


echo "" >> "ViewController_Extensions.swift"
echo "$NEW_LINE_1" >> "ViewController_Extensions.swift"
echo "$NEW_LINE_2" >> "ViewController_Extensions.swift"
echo "$NEW_LINE_3" >> "ViewController_Extensions.swift"
echo "$NEW_LINE_4" >> "ViewController_Extensions.swift"
echo "$NEW_LINE_5" >> "ViewController_Extensions.swift"
echo "$NEW_LINE_6" >> "ViewController_Extensions.swift"
echo "$NEW_LINE_7" >> "ViewController_Extensions.swift"


done


