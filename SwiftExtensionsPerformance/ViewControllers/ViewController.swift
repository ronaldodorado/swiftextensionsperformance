//
//  ViewController.swift
//  allenabled
//
//  Created by Ronaldo II Dorado on 2/12/16.
//  Copyright © 2016 Ronaldo II Dorado. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
    
    let stackView = UIStackView()
    let view1 = UIView()
    let view2 = UIView()
    let adapter = ViewAdapter()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        buildView()
        buildConstraints()
        setupStackView(stackView)
    }

    private func buildView() {
        view.backgroundColor = .white
        view.addSubview(stackView)
    }
    
    private func setupStackView(_ view: UIStackView) {
        view1.backgroundColor = .blue
        view2.backgroundColor = .gray
        
        stackView.axis = .vertical
        stackView.addArrangedSubview(view1)
        stackView.addArrangedSubview(view2)
        stackView.translatesAutoresizingMaskIntoConstraints = false;
        stackView.distribution = .fillProportionally
    }
    
    private func buildConstraints() {
        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
